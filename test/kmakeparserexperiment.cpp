/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kmakeparserexperiment.h"

#include <QString>
#include <QSet>
#include <QHash>
#include <QDebug>
#include <QRegularExpression>
#include <QFile>

KMakeParserExperiment::KMakeParserExperiment(QString root, KDotConfigParser *configParser)
    : KMakeParser(root, configParser)
{
}

KMakeParserExperiment::~KMakeParserExperiment()
{
}


QString KMakeParserExperiment::getNextLine()
{
    QString line = m_makefile->readLine();

    while (!m_makefile->atEnd()) {
        while (line.endsWith("\\n")) {
            line.remove("\\n");
            line += m_makefile->readLine();
        }

        if (!line.startsWith("#") && line != "\n")
            break;

        line = m_makefile->readLine();
    }

    return line;
}

QString KMakeParserExperiment::parseLine(const QString &line)
{
    QStringList tokenList = line.split(QRegularExpression("\\s+"), QString::SkipEmptyParts);
    QHash<QString, QStringList> pair;

    //qDebug() << "current line: " << line;
    //qDebug() << "token list: " << tokenList;

    int assignIndex = tokenList.indexOf(QRegularExpression(":?\\+?="));
    if (assignIndex > 0) {
        //qDebug() << "assignment: " << tokenList;
    }

    return "";
}

QChar skipCommentLine(QTextStream &ts) {
    QChar curChar('#');
    QChar nextChar;
    bool commentEnd = false;

    while (!commentEnd) {
        ts >> nextChar;
        if ((nextChar == '\n') && (curChar != '\\')) {
            commentEnd = true;
        }
        curChar = nextChar;
    }

    ts >> nextChar;
    return nextChar;
}

void parseIf()
{

}

void KMakeParserExperiment::parse()
{
    QString token;
    TokenType type;
    QStringList value;
    QString key;
    bool isKey = true;
    bool lineBreak;
    QHash<QString, QStringList> cache;

    while ((type = getNextToken(token, lineBreak)) != INVALID) {
        qDebug() << "current token: " << token;
        switch (type) {
            case IDENTIFIER:
                if (isKey)
                    key = token;
                else
                    value << token;
                break;
            case IF:
                parseIf();
                break;
            case ASSIGNMENT:
                isKey = false;
                if (!token.contains("+"))
                    value.clear();
                break;
            default:
                qWarning() << "invalid token: " << token;
        }

        if (lineBreak && !(key.isEmpty() || value.isEmpty())) {
            cache[key] = value;
            value.clear();
            key.clear();
            isKey = true;
        }
    }

    for (QHash<QString, QStringList>::iterator it = cache.begin(); it != cache.end(); it++) {
        qDebug() << it.key() << " : " << it.value();
    }
}

KMakeParserExperiment::TokenType KMakeParserExperiment::getNextToken(QString& token, bool &lineBreak)
{
    QChar qc;
    token.clear();
    lineBreak = false;

    while (!m_stream->atEnd()) {
        *m_stream >> qc;
        if (qc.isSpace())
            continue;

        while (qc == '#')
            qc = skipCommentLine(*m_stream);

        if (qc == '\\')
            *m_stream >> qc >> qc;

        while (!qc.isSpace()) {
            token += qc;
            *m_stream >> qc;
        }

        if (token.isEmpty())
            continue;

        if (qc == '\n')
            lineBreak = true;

        if (token.endsWith("="))
            return ASSIGNMENT;

        if (token.startsWith("if") || token.endsWith("if"))
            return IF;

        return IDENTIFIER;
    }

    return INVALID;
}
