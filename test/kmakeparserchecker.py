'''
Documentation, License etc.

@package kmakeparserchecker
'''

import sys
import os.path

# arguments:
# 1: dump to check
# 2: kernel directory

usageMsg = '''
Usage:
    kmakeparserchecker.py [dump file]

'''

if (len(sys.argv) != 2):
    print("argument error")
    print(usageMsg)
    exit -1;

dumpFile = sys.argv[1]

isDir = True

with open(dumpFile, 'r') as f:
    for line in f:
        if (line == '\n'):
            isDir = True
            continue

        line = line.strip('\n')

        if (isDir):
            # this is a base directory line
            baseDir = line
            isDir = False
            continue

        # this is item line
        if (line.endswith('.c')):
            fileName = line[: len(line) - 2];
            absPath = baseDir + '/' + fileName + '.o'
        else:
            absPath = baseDir + '/' + line

        if os.path.exists(absPath) == False:
            print('inconsistent entry:', absPath)

print('check completed')
