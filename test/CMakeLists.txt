set( kmakeparser_SRCS
    main.cpp
    ../src/kmakeparser.cpp
    ../src/kerneldotconfigfile.cpp
    ../src/utils.cpp
)

include_directories(
    /usr/include/kdevplatform
    ${CMAKE_HOME_DIRECTORY}/src
)

kconfig_add_kcfg_files(kmakeparser_SRCS ../src/kdevkernelconfig.kcfgc)
ki18n_wrap_ui(kmakeparser_SRCS ../src/ui/kdevkernelpreferences.ui)

add_executable( kmakeparser_test ${kmakeparser_SRCS} )

target_link_libraries( kmakeparser_test
    KF5::CoreAddons
    KF5::I18n
    Qt5::Widgets
    KDev::Util
    KDev::Project
)
