/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KMAKEPARSEREXPERIMENT_H
#define KMAKEPARSEREXPERIMENT_H

#include "kmakeparser.h"
#include "kdotconfigparser.h"

/**
 * @todo write docs
 */
class KMakeParserExperiment : private KMakeParser
{
public:
    /**
     * @todo write docs
     */
    explicit KMakeParserExperiment(QString root, KDotConfigParser *configParser);

    /**
     * @todo write docs
     */
    ~KMakeParserExperiment();

    enum TokenType {
        ASSIGNMENT,
        IF,
        IDENTIFIER,
        LINEBREAK,
        INVALID
    };

    /**
     * return a complete line, multi-line statement are merged
     */
    QString getNextLine();
    QString parseLine(const QString &line);
    TokenType getNextToken(QString &token, bool &lineBreak);
    void parse();

private:
    QFile *m_makefile;
    QTextStream *m_stream;

};

#endif // KMAKEPARSEREXPERIMENT_H
