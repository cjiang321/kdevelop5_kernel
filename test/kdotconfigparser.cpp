/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kdotconfigparser.h"

#include <util/path.h>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QRegularExpression>

KDotConfigParser::KDotConfigParser(const QString dotconfig)
{
    m_defines["__KERNEL__"] = "";

    parseDotConfig(KDevelop::Path(dotconfig));
}

KDotConfigParser::~KDotConfigParser()
{

}

void KDotConfigParser::parseDotConfig(const KDevelop::Path& dotconfig)
{
    QFile dfile(dotconfig.toLocalFile());
    static QRegularExpression defRegex("(\\w+)=(\"?[^\\n]+\"?)\n?");
    defRegex.optimize();

    bool archLine = m_arch.isEmpty();

    if (!dfile.open(QIODevice::ReadOnly))
        return;

    while (!dfile.atEnd()) {
        QString line(dfile.readLine());

        QRegularExpressionMatch defMatch = defRegex.match(line);
        if (!defMatch.hasMatch())
            continue;

        QString key(defMatch.captured(1));
        QString val(defMatch.captured(2));

        if (archLine) {
            archLine = false;
            m_arch = defMatch.captured(1).mid(7).toLower();
            qDebug() << "selected arch is: " << m_arch;
        }

        //TODO why convert to number string, this causes some error when parsing makefile
        if (val == "y" || val == "m")
            val = "1";
        else if (val == "n")
            val = "0";
        else if (val.startsWith('"') && val.endsWith('"'))
            val = val.mid(1, val.size() - 2);

        m_defines[key] = val;
    }

    dfile.close();
}

QString KDotConfigParser::defines(const QString& sym)
{
    return m_defines[sym];
}

QString KDotConfigParser::arch()
{
    return m_arch;
}

QHash<QString, QString> KDotConfigParser::defineList()
{
    return m_defines;
}

