/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDOTCONFIGPARSER_H
#define KDOTCONFIGPARSER_H

#include <QHash>

namespace KDevelop {
    class Path;
}

/**
 * @todo write docs
 */
class KDotConfigParser
{
public:
    /**
     * Default constructor
     */
    explicit KDotConfigParser(const QString dotconfig);

    /**
     * Destructor
     */
    ~KDotConfigParser();

    QString defines(const QString &sym);
    QString arch();
    QHash<QString, QString> defineList();

private:
    void parseDotConfig (const KDevelop::Path& dotconfig);

    QHash<QString, QString> m_defines;
    QString m_arch;
};

#endif // KDOTCONFIGPARSER_H
