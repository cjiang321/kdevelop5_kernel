/*
 Copyright (C) %{CURRENT_YEAR} by %{AUTHOR} <%{EMAIL}>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// application header
#include <QDebug>
#include "kmakeparser.h"
#include "kerneldotconfigfile.h"

int main(int argc, char **argv)
{
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    KDevelop::Path kernelPath("/home/jiangchao/workspace/ti-sdk/ti-linux");
    KernelDotConfigFile *configFile = new KernelDotConfigFile(kernelPath);
    Q_ASSERT_X(configFile->exists(), ".config exist", ".config is not present");
    configFile->parse();

    KMakeParser makeParser(kernelPath.toLocalFile(), configFile);

    Q_ASSERT_X(!makeParser.machDirs().isEmpty(), "mach directory", "no mach dir obtained");
    qInfo() << makeParser.machDirs();

    Q_ASSERT_X(configFile->defines("CONFIG_ARCH_OMAP2PLUS") == "1", "CONFIG_ARCH_OMAP2PLUS", "entry not defined");
    Q_ASSERT_X(configFile->defines("CONFIG_ARCH_OMAP") == "1", "CONFIG_ARCH_OMAP", "entry not defined");

    Q_ASSERT_X(makeParser.isValid(KDevelop::Path(kernelPath, "arch")), "arch", "arch is not valid");
    Q_ASSERT_X(makeParser.isValid(KDevelop::Path(kernelPath, "arch/arm")), "arch/arm", "arch/arm is not valid");

    return 0;
}
