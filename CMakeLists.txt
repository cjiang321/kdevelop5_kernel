cmake_minimum_required(VERSION 2.8.12)

project(kdevkernel)

find_package(ECM "5.14.0" REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMQtDeclareLoggingCategory)
include(ECMInstallIcons)
include(FeatureSummary)

find_package(KF5 REQUIRED COMPONENTS ItemModels)
find_package(KDevPlatform 5.1.40 REQUIRED)

add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(icons)

# kdebugsettings file
install(FILES kdevkernel.categories DESTINATION ${KDE_INSTALL_CONFDIR})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
