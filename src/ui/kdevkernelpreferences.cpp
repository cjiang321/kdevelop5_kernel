/*
 * when a project open, the configuration could be none-exist or exit
 * 1. none-exit
 *      in this case, it is necessary to populate config with proper values
 *      1. Arch: if kernel is configured, get arch from .config, otherwise pop windows for user to configure it
 *      2. Defconfig: this will need to be chosen as well if .config doesn't exits, if is exist, does call it "current config" and keep it
 *      3. cross compiler: this would be trivil unless to compile kernel target
 * 2. if exist, read the configuration, if some of the field doesn't exist, let the user choose it and set it immediately
 */

#include "kdevkernelpreferences.h"
#include "ui_kdevkernelpreferences.h"

#include "kdevkernelconfig.h"
#include "kdevkernel.h"

#include "utils.h"

#include <interfaces/iplugin.h>
#include <interfaces/icore.h>
#include <interfaces/iprojectcontroller.h>
#include <QDirIterator>
#include <QComboBox>

KDevKernelPreferences::KDevKernelPreferences(KDevelop::IPlugin* plugin, const KDevelop::ProjectConfigOptions& options, QWidget* parent)
    : ProjectConfigPage<KDevKernelSettings>(plugin, options, parent), KDevKernelUIBase(KDevKernelSettings::self(), project())
{
    setupUi(this);

    connect(m_prefsUi->kcfg_arch, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::activated), this, &KDevKernelPreferences::updated);
    connect(m_prefsUi->kcfg_defconfig, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::activated), this, &KDevKernelPreferences::updated);

    connect(m_prefsUi->compiler, &KUrlRequester::textChanged, this, &KDevKernelPreferences::updated);
    connect(m_prefsUi->compiler, &KUrlRequester::urlSelected, this, &KDevKernelPreferences::updated);

    connect(m_prefsUi->kcfg_module, &QCheckBox::toggled, this, &KDevKernelPreferences::updated);
    connect(m_prefsUi->kcfg_kernelDir, &KUrlRequester::textChanged, this, &KDevKernelPreferences::kernelDirChanged);
    connect(m_prefsUi->kcfg_kernelDir, &KUrlRequester::urlSelected, this, &KDevKernelPreferences::kernelDirChanged);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::textChanged, this, &KDevKernelPreferences::kernelDirChanged);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::urlSelected, this, &KDevKernelPreferences::kernelDirChanged);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::textChanged, this, &KDevKernelPreferences::updated);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::urlSelected, this, &KDevKernelPreferences::updated);
}

KDevKernelPreferences::~KDevKernelPreferences()
{
}

void KDevKernelPreferences::defaults()
{
    ProjectConfigPage::defaults();
}

void KDevKernelPreferences::reset()
{
    updateWidget();
}

void KDevKernelPreferences::apply()
{
    updateConfig();
    m_config->save();
    updateWidget();

    tryRemoveDotConfig();

    if (!dotConfigExists()) {
        plugin()->extension<KDevKernel>()->cacheValidFiles(project());
        plugin()->core()->projectController()->reparseProject(project(), true);
    }
}

void KDevKernelPreferences::updated()
{
    KDevKernelUtils::uiValueDump(m_prefsUi, __FILE__, __LINE__);
    if (isEntryValid()) {
        emit KDevKernelPreferences::changed();
    }
}

// FIXME
void KDevKernelPreferences::kernelDirChanged()
{
    populateArch();

    KernelDotConfigFile f(dotConfigPath());
    qInfo() << __FILE__ << __func__ << dotConfigPath();

    if (f.exists()) {
        f.parseArch();
        m_config->setArch(f.arch());
        m_prefsUi->kcfg_arch->setCurrentText(f.arch());

        populateDefconfig();
        m_prefsUi->kcfg_defconfig->setCurrentIndex(0);
        if (!m_config->defconfig().isEmpty())
            m_prefsUi->kcfg_defconfig->setCurrentText(m_config->defconfig());
    }

    updated();
}

QString KDevKernelPreferences::name() const
{
    return i18n("Linux Kernel");
}

QString KDevKernelPreferences::fullName() const
{
    return i18n("Configure Linux Kernel settings");
}

QIcon KDevKernelPreferences::icon() const
{
    return QIcon::fromTheme(QStringLiteral("run-build"));
}
