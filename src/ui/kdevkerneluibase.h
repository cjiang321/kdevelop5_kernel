/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDEVKERNELUIBASE_H
#define KDEVKERNELUIBASE_H

namespace KDevelop {
class IProject;
class Path;
}

namespace Ui {
class KDevKernelConfig;
}

class KDevKernelSettings;
class QString;
class QWidget;

/**
 * Common routines for both the preference panel and the import dialog
 */
class KDevKernelUIBase
{
public:
    /**
     * Default constructor
     */
    explicit KDevKernelUIBase(KDevKernelSettings *config, KDevelop::IProject *project);

    /**
     * Destructor
     */
    ~KDevKernelUIBase();

    void setupUi(QWidget *widget);

    bool tryRemoveDotConfig();
    bool dotConfigExists();

    bool isEntryValid();

    void setStatus(const QString& message, bool canApply);

    /**
     * Write UI values into Kconfig database
     */
    void updateConfig();

    /**
     * Read the Kconfig database and refresh UI accordingly
     */
    void updateWidget();

protected:
    /**
     * Populate the arch dropdown list
     */
    void populateArch();

    /**
     * Poluate the defconfig dropdown list accoridng to current arch selection in UI
     */
    void populateDefconfig();

    /**
     * Get the prefix string using compiler path
     */
    QString compilerPrefix(KDevelop::Path p);

    /**
     * From the GUI entry, get the path for .config file
     */
    KDevelop::Path dotConfigPath();

    Ui::KDevKernelConfig *m_prefsUi;
    KDevKernelSettings *m_config;
    KDevelop::IProject *m_project;

private:
    /**
     * Check the selected arch again the compiler arch from the triplets
     */
    bool validateArchCrosscompile();

    bool m_archChanged = false;
    bool m_configChanged = false;
    bool m_configValid = false;
    bool m_needToConfig = false;
    int m_archIndexCached = 0;
};

#endif // KDEVKERNELUIBASE_H
