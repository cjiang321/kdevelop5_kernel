/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kdevkernelimportdialog.h"
#include "ui_kdevkernelpreferences.h"
#include "kdevkernelconfig.h"
#include "kerneldotconfigfile.h"

#include <interfaces/iproject.h>
#include <interfaces/icore.h>
#include <interfaces/iruntime.h>
#include <interfaces/iruntimecontroller.h>
#include <QDialogButtonBox>

KDevKernelImportDialog::KDevKernelImportDialog(KDevKernelSettings *config, KDevelop::IProject *project, QWidget *parent)
    : QDialog(parent), KDevKernelUIBase(config, project)
{
    setWindowTitle(i18n("Configure Linux Kernel - %1", KDevelop::ICore::self()->runtimeController()->currentRuntime()->name()));

    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    m_buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    auto mainWidget = new QWidget(this);
    auto mainLayout = new QVBoxLayout;
    setLayout(mainLayout);
    mainLayout->addWidget(mainWidget);
    mainLayout->addWidget(m_buttonBox);

    setupUi(mainWidget);

    updateWidget();
    updated();

    connect(m_prefsUi->kcfg_arch, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::activated), this, &KDevKernelImportDialog::updated);
    connect(m_prefsUi->kcfg_defconfig, static_cast<void(QComboBox::*)(const QString&)>(&QComboBox::activated), this, &KDevKernelImportDialog::updated);

    connect(m_prefsUi->compiler, &KUrlRequester::textChanged, this, &KDevKernelImportDialog::updated);
    connect(m_prefsUi->compiler, &KUrlRequester::urlSelected, this, &KDevKernelImportDialog::updated);

    connect(m_prefsUi->kcfg_module, &QCheckBox::toggled, this, &KDevKernelImportDialog::updated);
    connect(m_prefsUi->kcfg_kernelDir, &KUrlRequester::textChanged, this, &KDevKernelImportDialog::kernelDirChanged);
    connect(m_prefsUi->kcfg_kernelDir, &KUrlRequester::urlSelected, this, &KDevKernelImportDialog::kernelDirChanged);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::textChanged, this, &KDevKernelImportDialog::kernelDirChanged);
    connect(m_prefsUi->kcfg_buildDir, &KUrlRequester::urlSelected, this, &KDevKernelImportDialog::kernelDirChanged);
}

KDevKernelImportDialog::~KDevKernelImportDialog()
{
}

//TODO consider the scenario when config is valid and need reset
void KDevKernelImportDialog::updated()
{
    bool canApply = isEntryValid();

    auto okButton = m_buttonBox->button(QDialogButtonBox::Ok);
    okButton->setEnabled(canApply);
    if (canApply) {
        auto cancelButton = m_buttonBox->button(QDialogButtonBox::Cancel);
        cancelButton->clearFocus();
        updateConfig();
    }
}

void KDevKernelImportDialog::kernelDirChanged()
{
    populateArch();

    KernelDotConfigFile f(dotConfigPath());
    qInfo() << __FILE__ << __func__ << dotConfigPath();

    if (f.exists()) {
        f.parseArch();
        // make arch got from .config the original arch, so m_archChanged will reflect the difference between user selection and this value
        m_config->setArch(f.arch());
        m_prefsUi->kcfg_arch->setCurrentText(f.arch());

        populateDefconfig();
        m_prefsUi->kcfg_defconfig->setCurrentIndex(0);
        // make this config the original defconfig, so m_defconfigChanged will reflect the difference between user selection and this value
        m_config->setDefconfig(m_prefsUi->kcfg_defconfig->currentText());

        // TODO as this is existing config, the base defconfig will be unknown
        // if (!m_config->defconfig().isEmpty())
        //     m_prefsUi->kcfg_defconfig->setCurrentText(m_config->defconfig());
    }

    updated();
}
