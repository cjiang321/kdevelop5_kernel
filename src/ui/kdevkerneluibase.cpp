/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kdevkerneluibase.h"

#include "ui_kdevkernelpreferences.h"
#include "kdevkernelconfig.h"
#include "utils.h"

#include <interfaces/iproject.h>
#include <util/path.h>
#include <QWidget>
#include <QComboBox>
#include <KUrlRequester>
#include <KColorScheme>


KDevKernelUIBase::KDevKernelUIBase(KDevKernelSettings *config, KDevelop::IProject *project)
    : m_prefsUi(new Ui::KDevKernelConfig), m_config(config), m_project(project)
{
}

KDevKernelUIBase::~KDevKernelUIBase()
{
    delete m_prefsUi;
}

void KDevKernelUIBase::setupUi(QWidget *widget)
{
    m_prefsUi->setupUi(widget);
}

void KDevKernelUIBase::populateArch()
{
    m_prefsUi->kcfg_arch->setCurrentIndex(-1);

    QDir d(m_prefsUi->kcfg_kernelDir->text() + "/arch");
    if (!d.exists())
        return;

    d.setFilter(QDir::NoDotAndDotDot | QDir::Dirs);

    m_prefsUi->kcfg_arch->clear();;

    for (int i = 0; i < d.entryList().size(); i++)
        m_prefsUi->kcfg_arch->insertItem(i, d.entryList()[i]);
}

void KDevKernelUIBase::populateDefconfig()
{
    KDevelop::Path archPath(m_prefsUi->kcfg_kernelDir->text() + QString("/arch/%1").arg(m_prefsUi->kcfg_arch->currentText()));
    QDir archDir(archPath.toLocalFile());

    KDevelop::Path configPath(archPath, "configs");
    QDir configDir(configPath.toLocalFile());

    QStringList nameFilter({"*defconfig"});
    archDir.setNameFilters(nameFilter);
    configDir.setNameFilters(nameFilter);

    int i = 0;
    m_prefsUi->kcfg_defconfig->clear();

    if (!m_archChanged && dotConfigExists())
        m_prefsUi->kcfg_defconfig->insertItem(i++, "current config");

    foreach(QString item, archDir.entryList() + configDir.entryList()) {
        QString configName = item.left(item.size() - 9); // strip "defconfig"
        if (configName.isEmpty())
            configName = "default_";
        m_prefsUi->kcfg_defconfig->insertItem(i++, configName.left(configName.size() - 1)); // strip "_"
    }
}

bool KDevKernelUIBase::validateArchCrosscompile()
{
    static QRegExp triplet("(\\w*)-?(.*)gcc");
    KDevelop::Path compilerPath(m_prefsUi->compiler->text());
    QString compilerArch;

    if (triplet.exactMatch(compilerPath.lastPathSegment())) {
        compilerArch = triplet.cap(1).isEmpty() ? "x86" : triplet.cap(1);
    }

    return (m_prefsUi->kcfg_arch->currentText() == compilerArch);
}

QString KDevKernelUIBase::compilerPrefix(KDevelop::Path p)
{
    static QRegExp triplet("(\\w*)-?(.*)gcc");

    QString prefix("");
    if (triplet.exactMatch(p.lastPathSegment())) {
        if (!triplet.cap(1).isEmpty()) {
            prefix = p.toLocalFile().left(p.toLocalFile().size() - 3);
        }
    }

    return prefix;
}

void KDevKernelUIBase::updateConfig()
{
    m_config->setArch(m_prefsUi->kcfg_arch->currentText());
    m_config->setDefconfig(m_prefsUi->kcfg_defconfig->currentText());
    m_config->setCrossPrefix(compilerPrefix(KDevelop::Path(m_prefsUi->compiler->text())));

    m_config->setModule(m_prefsUi->kcfg_module->isChecked());
    m_config->setBuildDir(m_prefsUi->kcfg_buildDir->text());
    m_config->setKernelDir(m_prefsUi->kcfg_kernelDir->text());

    m_config->setValidFiles(QStringList()); // TODO implement this feature
}

void KDevKernelUIBase::updateWidget()
{
    m_prefsUi->kcfg_module->setChecked(m_config->module());
    m_prefsUi->kcfg_buildDir->setText(m_config->buildDir());
    m_prefsUi->kcfg_kernelDir->setText(m_config->kernelDir());

    populateArch();

    m_prefsUi->kcfg_arch->setCurrentIndex(-1);
    if (!m_config->arch().isEmpty()) {
        m_prefsUi->kcfg_arch->setCurrentText(m_config->arch());
        populateDefconfig();
    }

    m_prefsUi->kcfg_defconfig->setCurrentIndex(-1);
    if (dotConfigExists())  // set to current config
        m_prefsUi->kcfg_defconfig->setCurrentIndex(0);
    if (!m_config->defconfig().isEmpty())
        m_prefsUi->kcfg_defconfig->setCurrentText(m_config->defconfig());

    m_prefsUi->compiler->setText(m_config->crossPrefix() + "gcc");
    if (m_config->crossPrefix().isEmpty())
        m_prefsUi->compiler->setText(QStandardPaths::findExecutable("gcc"));
}

bool KDevKernelUIBase::isEntryValid()
{
    KDevKernelUtils::uiValueDump(m_prefsUi, __FILE__, __LINE__);

    setStatus("configuration OK", true);

    m_archChanged = m_prefsUi->kcfg_arch->currentText() != m_config->arch();
    if (m_archChanged) {
        // re-populate defconfig list if current selection is not the same as previous selection(cached value)
        // also as this check is guarded by m_archChanged, this cached value will not be used when inital arch is not modified
        if (m_prefsUi->kcfg_arch->currentIndex() != m_archIndexCached) {
            m_archIndexCached = m_prefsUi->kcfg_arch->currentIndex();
            populateDefconfig();
        }
        setStatus("arch changed, kernel will be reconfigured", true);
    }

    m_configChanged = m_prefsUi->kcfg_defconfig->currentText() != m_config->defconfig();
    if (m_configChanged) {
        setStatus("defconfig changed, kernel will be reconfigured", true);
    }

    m_configValid = validateArchCrosscompile();

    if (!m_configValid)
        setStatus("Select matching arch and compiler!", false);

    return m_configValid;
}

void KDevKernelUIBase::setStatus(const QString& message, bool canApply)
{
    KColorScheme scheme(QPalette::Normal);
    KColorScheme::ForegroundRole role;
    if (canApply) {
        role = KColorScheme::PositiveText;
    } else {
        role = KColorScheme::NegativeText;
    }
    m_prefsUi->status->setText(QStringLiteral("<i><font color='%1'>%2</font></i>").arg(scheme.foreground(role).color().name()).arg(message));
}

KDevelop::Path KDevKernelUIBase::dotConfigPath()
{
    if (m_prefsUi->kcfg_buildDir->text().isEmpty())
        return KDevelop::Path(m_prefsUi->kcfg_kernelDir->text());

    return KDevelop::Path(m_prefsUi->kcfg_buildDir->text());
}

bool KDevKernelUIBase::dotConfigExists()
{
    QFile f(KDevelop::Path(dotConfigPath(), ".config").toLocalFile());
    return f.exists();
}

bool KDevKernelUIBase::tryRemoveDotConfig()
{
    if (m_archChanged || m_configChanged) {
        qInfo() << __func__ << "going to remove .config";
        QFile f(KDevelop::Path(dotConfigPath(), ".config").toLocalFile());
        f.remove();
        return true;
    }

    return false;
}
