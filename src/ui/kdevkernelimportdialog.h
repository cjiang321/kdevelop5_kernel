/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDEVKERNELIMPORTDIALOG_H
#define KDEVKERNELIMPORTDIALOG_H

#include "kdevkerneluibase.h"
#include <QDialog>

class QDialogButtonBox;

namespace Ui {
class KDevKernelConfig;
}

namespace KDevelop {
class IProject;
}

/**
 * @todo write docs
 */
class KDevKernelImportDialog : public QDialog, public KDevKernelUIBase
{
    Q_OBJECT

public:
    explicit KDevKernelImportDialog(KDevKernelSettings *config, KDevelop::IProject *project, QWidget *parent = nullptr);

    ~KDevKernelImportDialog();

private Q_SLOTS:
    void updated();
    void kernelDirChanged();

private:
    QDialogButtonBox* m_buttonBox;
    KDevelop::IProject *m_project;
};

#endif // KDEVKERNELIMPORTDIALOG_H
