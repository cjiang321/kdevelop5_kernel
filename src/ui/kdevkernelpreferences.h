

#ifndef KDEVKERNELPREFERENCES_H
#define KDEVKERNELPREFERENCES_H

#include <project/projectconfigpage.h>
#include "kdevkerneluibase.h"

class KDevKernelPreferences : public ProjectConfigPage<KDevKernelSettings>, public KDevKernelUIBase
{
    Q_OBJECT

public:
    explicit KDevKernelPreferences(KDevelop::IPlugin *plugin, const KDevelop::ProjectConfigOptions &options, QWidget *parent = nullptr);

    ~KDevKernelPreferences() override;

    void reset() override;
    void apply() override;
    void defaults() override;

    QString name() const override;
    QString fullName() const override;
    QIcon icon() const override;

private Q_SLOTS:
    void updated();
    void kernelDirChanged();
};

#endif
