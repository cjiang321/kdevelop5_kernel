/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"
#include "kmakeparser.h"

#include <util/path.h>
#include <QDebug>
#include <QDirIterator>
#include <QRegularExpression>
#include <QFile>

KMakeParser::KMakeParser(const QString& root, KernelDotConfigFile *dotConfig) : m_dotConfig(dotConfig)
{
    Q_ASSERT(!m_dotConfig->arch().isEmpty());
    m_data.validFiles[KDevelop::Path(root)].validFiles += "arch"; // arch is always valid
    m_data.validFiles[KDevelop::Path(root + "/arch")].validFiles += m_dotConfig->arch();
    parseRootMakefile(KDevelop::Path(root));
    parseDirectory(KDevelop::Path(root));
}

KMakeParser::~KMakeParser()
{
}

bool KMakeParser::isValid(const KDevelop::Path& url) const
{
    KDevelop::Path containingDir(url.parent());
    const ValidFileList &validFiles(m_data.validFiles[containingDir]);
    QString file(url.lastPathSegment());

    if (validFiles.validFiles.contains(file))
        return true;

    return false;
}

QStringList KMakeParser::machDirs() const
{
    return m_data.machDirs;
}

QString KMakeParser::getNextLine(QFile &file)
{
    QString ret(file.readLine());

    while (ret.endsWith("\\\n"))
        ret += file.readLine();

    return ret;
}

void KMakeParser::mergeInfoGlobalValid(const KDevelop::Path &dir, const ValidFileList& vf)
{
    m_data.validFiles[dir].lastUpdate = vf.lastUpdate;

    foreach (const QString &file, vf.validFiles) {
        // Sometimes files are referenced that are several directories below
        if (file.contains('/')) {
            KDevelop::Path absFilePath(dir, file);
            KDevelop::Path parent(absFilePath.parent());
            m_data.validFiles[parent].validFiles << absFilePath.lastPathSegment();

            while (parent != dir) {
                QString f(parent.lastPathSegment());
                KDevelop::Path d(parent.parent());
                m_data.validFiles[d].validFiles << f;
                parent = d;
            }

            continue;
        }

        m_data.validFiles[dir].validFiles += file;
    }
}

void KMakeParser::parseDirectory(const KDevelop::Path &pdir)
{
    QString archDir(QString("arch/%1").arg(m_dotConfig->arch()));

    ValidFileList &validFiles = m_data.validFiles[pdir];

    foreach (const QString &file, validFiles.validFiles) {
        KDevelop::Path filePath(pdir, file);
        if (QFileInfo(filePath.toLocalFile()).isDir()) {
            if (filePath.toLocalFile().endsWith(archDir))
                parseRootMakefile(filePath);
            ValidFileList localValidFiles;
            parseMakefile(filePath, localValidFiles);
            parseDirectory(filePath);
            mergeInfoGlobalValid(pdir, localValidFiles);
        }
    }
}

void KMakeParser::parseRootMakefile(const KDevelop::Path &root)
{
    static QRegularExpression objy("^\\w+-y[+:= \t]*(\\w+\\/.*)\\n");
    static QRegularExpression spTab("\t| ");
    objy.optimize();
    spTab.optimize();

    ValidFileList &rootFiles = m_data.validFiles[root];

    QFile makefile((KDevelop::Path( root, "Makefile")).toLocalFile());

    if (!makefile.exists() || !makefile.open(QIODevice::ReadOnly)) {
        qWarning() << "cannot open makefile in project root" << makefile;
        return;
    }

    while (!makefile.atEnd()) {
        QString line(getNextLine(makefile));

        QRegularExpressionMatch match = objy.match(line);

        if (!match.hasMatch())
            continue;

        QStringList dirs(match.captured(1).split(spTab, QString::SkipEmptyParts));
        foreach (const QString &d, dirs) {
            QUrl url(d);
            QString dirNanme = url.adjusted(QUrl::StripTrailingSlash).fileName();
            rootFiles.validFiles << dirNanme;
            qDebug() << __func__ << root << dirNanme;
        }
    }

    makefile.close();
}

/*
 * first line is would be parse by parseObj, the function only pass subsequent lines
 * TODO consider overload readLine() and merge multi-line into single line
 */
void KMakeParser::parseMultiLineDependencies(QFile& makefile, QStringList& objList)
{
    bool endOfMultiLine = false;

    while (!endOfMultiLine) {
        QString line = makefile.readLine();
        if (!line.endsWith("\\\n"))
            endOfMultiLine = true;
        line.remove("\\\n");    // for multi-lines
        line.remove("\n");      // for last line of the multi-line
        objList += line.split(QRegularExpression("\t| "), QString::SkipEmptyParts);
    }
}

void KMakeParser::parseDependencies(const QRegularExpressionMatch &match, QStringList& objFiles, QMap<QString, QStringList> &objCache)
{
    static QRegularExpression spTab("\t| ");
    spTab.optimize();

    QString y(match.captured(2));
    y.replace("${", "$(");
    y.replace("}", ")");
    if (y.startsWith("$(") && y.endsWith(")")) {
        QString defEntry(m_dotConfig->defines(y.mid(2, y.size() - 3)));
        if (defEntry == "1")
            y = "y";
    }

    if (y != "y" && y != "objs" && y != "")
        return;

    QStringList objList(match.captured(4).split(spTab, QString::SkipEmptyParts));

    if (match.captured(1) == "machine" || match.captured(1) == "plat") {
        foreach (const QString &obj, objList) {
            QString pDir((match.captured(1) == "machine" ? "mach-" : "plat-") + obj);
            objFiles += pDir + "/";
            m_data.machDirs << pDir;
        }

        return;
    }

    if (match.captured(1) == "obj") {
        objFiles += objList;
    }
    else {
        QStringList tmpFile;
        foreach (const QString &obj, objList) {
            if (obj.startsWith("$(")) {
                tmpFile += objCache[obj.mid(2, obj.size() - 3)];
                continue;
            }
            tmpFile += obj;
        }

        if (match.captured(3) == "+")
            objCache[match.captured(1)] += tmpFile;
        else
            objCache[match.captured(1)] = tmpFile;
    }
}


bool KMakeParser::parseIfdef(const QRegularExpressionMatch &matchIf, const bool isIfdef)
{
    QString def(m_dotConfig->defines(matchIf.captured(2)));
    bool ifTrue;

    if (def == "1")
        def = "y";

    ifTrue = (def == matchIf.captured(3));
    if (isIfdef)
        ifTrue = (def == "y");

    if (matchIf.captured(1) == "n")
        ifTrue = !ifTrue;

    return ifTrue;
}

void KMakeParser::substituteObj(const KDevelop::Path dir, QString &file)
{
    QString archDir(QString("arch/%1").arg(m_dotConfig->arch()));
    ValidFileList &validFiles = m_data.validFiles[dir];

    if (file.endsWith(".o"))
        file = file.left(file.size() - 2) + ".c";
    else if (file.endsWith(".dtb"))
        file = file.left(file.size() - 4) + ".dts"; // TODO do we need to parse this at all?
    else if (file.endsWith("/"))
        file = file.left(file.size() - 1);
    else {
        qDebug() << "unrecognized file" << dir << file;
        return;
    }

    // Some directories are specified from the source root in the arch dir
    if (dir.toLocalFile().endsWith(archDir) && file.startsWith(archDir))
        file = file.mid(archDir.size() + 1);

    validFiles.validFiles << file;
}

/*
 * stage 1: generate a list of objects that should be include in the build
 * stage 2: from the objs, generate the corresponding source file
 */
void KMakeParser::parseMakefile(const KDevelop::Path& dir, ValidFileList &validFiles)
{
    static QRegularExpression ifdefExp("if(n?)def (\\w+)\\n");
    static QRegularExpression ifeqExp("if(n?)eq \\(\\$\\((\\w+)\\),(\\w*) *\\)\\n");
    static QRegularExpression objyExp("[ \t]*([\\w-]+)-([^+:= \t]*)[\t ]*(\\+?:?)=([^\\\\]+)\\\\?\n");

    ifdefExp.optimize();
    ifeqExp.optimize();
    objyExp.optimize();

    validFiles.lastUpdate = QDateTime::currentDateTime();

    QFile makefile(KDevelop::Path(dir, "Makefile").toLocalFile());

    if (!makefile.exists() || !makefile.open(QIODevice::ReadOnly)) {
        qWarning() << "cannot open " << makefile;
        return;
    }

    QStringList objFiles;
    QMap<QString, QStringList> objCache;    //TODO considering using qhash instead
    QList<bool> ifStack;

    while (!makefile.atEnd()) {
        QString line(getNextLine(makefile));

        if (!ifStack.isEmpty()) {
            if (line == "endif\n") {
                ifStack.pop_back();
                continue;
            }

            if (line == "else\n") {
                ifStack.back() = !ifStack.back();
                continue;
            }

            if (!ifStack.back())
                continue;
        }

        QRegularExpressionMatch objyMatch = objyExp.match(line);
        if (objyMatch.hasMatch()) {
            parseDependencies(objyMatch, objFiles, objCache);
            continue;
        }

        QRegularExpressionMatch ifdefMatch = ifdefExp.match(line);
        if (ifdefMatch.hasMatch()) {
            ifStack.push_back(parseIfdef(ifdefMatch, true));
            continue;
        }

        QRegularExpressionMatch ifeqMatch = ifeqExp.match(line);
        if (ifeqMatch.hasMatch()) {
            ifStack.push_back(parseIfdef(ifeqMatch, false));
            continue;
        }
    }

    makefile.close();

    QString archDir(QString("arch/%1/").arg(m_dotConfig->arch()));

    foreach (QString file, objFiles) {
        if (file.endsWith(".o") && objCache.contains(file.left(file.size() - 2))) {
            qDebug() << __func__ << "file is in cache, needs substitute: " << file;
            foreach(QString f, objCache[file.left(file.size() - 2)])
                substituteObj(dir, f);
            continue;
        }

        substituteObj(dir, file);
    }
}
