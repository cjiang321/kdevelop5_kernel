
#include "kmakeparser.h"
#include "kdevkernelconfig.h"
#include "kdevkernelconfigdef.h"
#include "ui_kdevkernelpreferences.h"

#include <util/path.h>

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QHash>

namespace KDevKernelUtils
{
void dumpDefines(const QHash< QString, QString >& defines)
{
    QFile f("definedump.txt");

    if (!f.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qWarning() << "cannot open file to dump";
        return;
    }

    QTextStream out(&f);

    QHash< QString, QString >::const_iterator i = defines.constBegin();
    while (i != defines.constEnd()) {
        out << i.key() << " : " << i.value() << "\n";
        i++;
    }
    f.close();
}

void dumpValidFiles(const QMap<KDevelop::Path, ValidFileList> &validFiles)
{
    QFile f("validfiledump.txt");
    if (!f.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qWarning() << "cannot open file to dump";
        return;
    }

    QTextStream out(&f);

    QMap<KDevelop::Path, ValidFileList>::const_iterator i = validFiles.constBegin();
    while (i != validFiles.constEnd()) {
        out << i.key().toLocalFile() << "\n";
        foreach (const QString &f, i.value().validFiles) {
            out << f << "\n";
        }
        out << "\n";
        i++;
    }
    f.close();
}

void kdevkernelConfigDump(const KDevKernelSettings *config, const char *file = __FILE__, const int line = __LINE__)
{
    qInfo() << file << ":" << line << " [config dump]";
    qInfo() << "arch: " << config->arch();
    qInfo() << "config: " << config->defconfig();
    qInfo() << "crossPrefix: " << config->crossPrefix();

    qInfo() << "module: " << config->module();
    qInfo() << "builddir: " << config->buildDir();
    qInfo() << "kerneldir: " << config->kernelDir();
}

void kdevkernelConfigDump(const KConfigGroup group, const char *file = __FILE__, const int line = __LINE__)
{
    qInfo() << file << ":" << line << " [config dump]";
    qInfo() << "arch: " << group.readEntry(KERN_ARCH);
    qInfo() << "config: " << group.readEntry(KERN_DEFCONFIG);
    qInfo() << "crossPrefix: " << group.readEntry(KERN_CROSS);

    qInfo() << "module: " << group.readEntry(KERN_MODULE);
    qInfo() << "builddir: " << group.readEntry(KERN_BDIR);
    qInfo() << "kerneldir: " << group.readEntry(KERN_KDIR);
}

void uiValueDump(Ui::KDevKernelConfig *ui, const char *file = __FILE__, const int line = __LINE__)
{
    qInfo() << file << ":" << line << " [ui dump]";
    qInfo() << "arch: " << ui->kcfg_arch->currentText();
    qInfo() << "config: " << ui->kcfg_defconfig->currentText();
    qInfo() << "compiler: " << ui->compiler->text();

    qInfo() << "module: " << ui->kcfg_module->isChecked();
    qInfo() << "builddir: " << ui->kcfg_buildDir->text();
    qInfo() << "kerneldir: " << ui->kcfg_kernelDir->text();
}
}
