/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kerneldotconfigfile.h"
#include "kdevkernelconfigdef.h"

#include <KConfigGroup>

#include <QRegularExpression>
#include <QFile>
#include <QDebug>

KernelDotConfigFile::KernelDotConfigFile(const KDevelop::Path parentPath) : m_path(parentPath)
{
}

KernelDotConfigFile::~KernelDotConfigFile()
{

}

bool KernelDotConfigFile::setPath(KDevelop::Path parentPath)
{
    if (m_path == parentPath)
        return false;

    m_path = parentPath;
    m_arch.clear();
    m_defines.clear();
    return true;
}

bool KernelDotConfigFile::exists() const
{
    return QFile(m_path.toLocalFile() + "/.config").exists();
}

QString KernelDotConfigFile::defines(const QString& symbol) const
{
    return m_defines[symbol];
}

QString KernelDotConfigFile::arch() const
{
    return m_arch;
}

QHash<QString, QString> KernelDotConfigFile::defineList() const
{
    return m_defines;
}

void KernelDotConfigFile::parse()
{
    QRegularExpression defExpr("(\\w+)=(\"?[^\\n]+\"?)\n?");
    defExpr.optimize();

    m_defines["__KERNEL__"] = "";

    QFile file(m_path.toLocalFile() + "/.config");
    if (!file.open(QIODevice::ReadOnly))
        return;

    m_arch.clear();

    while (!file.atEnd()) {
        QString line(file.readLine());

        QRegularExpressionMatch defMatch = defExpr.match(line);
        if (!defMatch.hasMatch())
            continue;

        QString key(defMatch.captured(1));
        QString val(defMatch.captured(2));

        if (m_arch.isEmpty()) {
            m_arch = defMatch.captured(1).mid(7).toLower();
            qDebug() << "selected arch is: " << m_arch;
        }

        //TODO why convert to number string, this causes some error when parsing makefile
        if (val == "y" || val == "m")
            val = "1";
        else if (val == "n")
            val = "0";
        else if (val.startsWith('"') && val.endsWith('"'))
            val = val.mid(1, val.size() - 2);

        m_defines[key] = val;
    }

    file.close();
}

void KernelDotConfigFile::parseArch()
{
    QRegularExpression defExpr("(\\w+)=(\"?[^\\n]+\"?)\n?");
    defExpr.optimize();

    QFile file(m_path.toLocalFile() + "/.config");
    if (!file.open(QIODevice::ReadOnly))
        return;

    m_arch.clear();

    while (!file.atEnd()) {
        QString line(file.readLine());

        QRegularExpressionMatch defMatch = defExpr.match(line);
        if (!defMatch.hasMatch())
            continue;

        if (m_arch.isEmpty()) {
            m_arch = defMatch.captured(1).mid(7).toLower();
            qDebug() << "selected arch is: " << m_arch;
            break;
        }
    }

    file.close();
}

bool KernelDotConfigFile::updateConfig(KConfigGroup configGrp)
{
    // when parse is not executed
    if (m_arch.isEmpty())
        return false;

    bool isArchChanged = false;

    if (configGrp.readEntry(KERN_ARCH) != m_arch) {
        configGrp.writeEntry(KERN_ARCH, m_arch);
        isArchChanged = true;
    }

    // in case .config exist, simply update kconfig entry to current config
    if (configGrp.readEntry(KERN_DEFCONFIG).isEmpty())
        configGrp.writeEntry(KERN_DEFCONFIG, "current config");

    return isArchChanged;
}
