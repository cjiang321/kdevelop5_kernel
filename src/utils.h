
#ifndef KDEVKERNEL_UTILS
#define KDEVKERNEL_UTILS

#include <QHash>

namespace KDevelop {
    class Path;
}

namespace Ui {
    class KDevKernelConfig;
}

class KDevKernelSettings;
class KConfigGroup;
struct ValidFileList;

namespace KDevKernelUtils
{
void dumpDefines(const QHash< QString, QString >& defines);
void dumpValidFiles(const QMap<KDevelop::Path, ValidFileList> &validFiles);
void kdevkernelConfigDump(const KDevKernelSettings *config, const char *file = __FILE__, const int line = __LINE__);
void kdevkernelConfigDump(const KConfigGroup group, const char *file = __FILE__, const int line = __LINE__);
void uiValueDump(Ui::KDevKernelConfig *ui, const char *file = __FILE__, const int line = __LINE__);
}

#endif
