/*
 *  Copyright (C) 2012 Alexandre Courbot <gnurou@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDEVKERNELCONFIGDEF_H
#define KDEVKERNELCONFIGDEF_H

#define KERN_KGROUP "Kernel"

#define KERN_ARCH "Arch"
#define KERN_DEFCONFIG "Kernel Config"
#define KERN_CROSS "Cross-compiler Prefix"

#define KERN_MODULE "Kernel Module"
#define KERN_KDIR "Kernel Directory"
#define KERN_BDIR "Build Directory"

#define KERN_VALIDFILES "User Valid Files"

#endif
