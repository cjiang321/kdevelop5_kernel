/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KMAKEPARSER_H
#define KMAKEPARSER_H

#include "kerneldotconfigfile.h"

#include <QSet>
#include <QMap>
#include <QDateTime>
#include <QFile>


struct ValidFileList
{
    QDateTime lastUpdate;
    QSet<QString> validFiles;
};

struct KMakeData
{
    QStringList machDirs;
    QMap<KDevelop::Path, ValidFileList> validFiles;
};

/**
 * Parser for Linux Kenerl's Kbuild system
 */
class KMakeParser
{
public:
    /**
     * Default constructor
     */
    explicit KMakeParser(const QString &root, KernelDotConfigFile *dotConfig);

    /**
     * Destructor
     */
    ~KMakeParser();

    /**
     * Check if the given file is include in the current build
     */
    bool isValid(const KDevelop::Path& url) const;

    /**
     * Get the machine directories, useful for generating a list of include path
     */
    QStringList machDirs() const;

private:

    /**
     * Read next line, if \n is escaped, concatenate the next line as well
     */
    QString getNextLine(QFile &file);

    /**
     * Parse the makefile in the kernel src root or the arch root
     */
    void parseRootMakefile (const KDevelop::Path& root);

    /**
     * Recursively parse all children directories in the current directory
     */
    void parseDirectory(const KDevelop::Path &pdir);

    /**
     * Parse a regular makefile, except for the ones in the root directories
     */
    void parseMakefile(const KDevelop::Path& dir, ValidFileList &validFiles);

    /**
     * Parse the if statement in the makefile
     */
    bool parseIfdef(const QRegularExpressionMatch &matchIf, const bool isIfdef);

    /**
     * Parse object dependencies, if the statement spans over multiple lines, only the first line is parsed in this method
     */
    void parseDependencies(const QRegularExpressionMatch &match, QStringList& objFiles, QMap<QString, QStringList> &objCache);

    /**
     * Parse object dependencies, for lines other than the first line
     */
    void parseMultiLineDependencies(QFile &makefile, QStringList &objList);

    /**
     * Dependency evaluation, all built-in objects should starts with "obj"
     */
    void substituteObj(const KDevelop::Path dir, QString &file);

    void mergeInfoGlobalValid(const KDevelop::Path &dir, const ValidFileList& vf);

    /**
     * Parse the definition passed as compiler flags
     */
    void parseCompilerDefine(const QRegularExpressionMatch &match);

    KMakeData m_data;
    KernelDotConfigFile *m_dotConfig;
};

#endif // KMAKEPARSER_H
