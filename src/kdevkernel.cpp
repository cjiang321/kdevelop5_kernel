#include "kdevkernel.h"
#include "kdevkernelpreferences.h"
#include "kdevkernelimportdialog.h"
#include "kdevkernelconfigdef.h"
#include "kdevkernelconfig.h"
#include "utils.h"

#include <debug.h>
#include <interfaces/icore.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/iplugincontroller.h>
#include <interfaces/iproject.h>
#include <project/projectmodel.h>
#include <KPluginFactory>
#include <KProcess>
#include <QFileInfo>
#include <QDirIterator>
#include <util/path.h>

K_PLUGIN_FACTORY_WITH_JSON(KDevKernelFactory, "kdevkernel.json", registerPlugin<KDevKernel>(); )

KDevKernel::KDevKernel(QObject *parent, const QVariantList& args)
    : KDevelop::AbstractFileManagerPlugin(QStringLiteral("kdevkernel"), parent)
{
    Q_UNUSED(args);

    qInfo(PLUGIN_KDEVKERNEL) << "kdevkernel is loaded!"; // DEBUG ONLY, DELETE THIS

    KDevelop::IPlugin *i = core()->pluginController()->pluginForExtension("org.kdevelop.IMakeBuilder");

    if (!i) {
        setErrorDescription(i18n("Unable to obtain make builder"));
        return;
    }

    m_builder = i->extension<IMakeBuilder>();
    Q_ASSERT(m_builder);

    connect(KDevelop::ICore::self()->projectController(), &KDevelop::IProjectController::projectClosing, this, &KDevKernel::projectClosing);
}

KDevKernel::~KDevKernel()
{
}

void importViaDialog(KDevelop::IProject *project)
{
    KDevKernelSettings::instance(project->projectConfiguration());

    KDevKernelUtils::kdevkernelConfigDump(KDevKernelSettings::self(), __FILE__, __LINE__); // DEBUG ONLY, DELETE THIS

    KDevKernelImportDialog id(KDevKernelSettings::self(), project);

    if (id.exec()) {
        id.tryRemoveDotConfig();

        KDevKernelUtils::kdevkernelConfigDump(KDevKernelSettings::self(), __FILE__, __LINE__); // DEBUG ONLY, DELETE THIS

        KDevKernelSettings::self()->save();

        project->projectConfiguration()->group(KERN_KGROUP).writeEntry(KERN_MODULE ,KDevKernelSettings::self()->module());
    }

    delete KDevKernelSettings::self();
}

KDevelop::ProjectFolderItem * KDevKernel::import(KDevelop::IProject* project)
{
    qInfo() << __FILE__ << __func__; // DEBUG ONLY, DELETE THIS

    // This effectively cleans up everything
    projectClosing(project);

    KSharedConfigPtr configPtr = project->projectConfiguration();
    // Force language to "C"
    configPtr->group("Project").writeEntry("Language", "C");
    // Force disabling of make-based include path resolver
    configPtr->group("MakeBuilder").writeEntry("Resolve Using Make", false);
    // Init non-module project's kernel path when kconfig entry is not present
    if (!configPtr->group(KERN_KGROUP).hasKey(KERN_KDIR)) {
        if (!isModuleProject(project))
            configPtr->group(KERN_KGROUP).writeEntry(KERN_KDIR, project->path().toLocalFile());
    }

    bool configChanged = true;

    dotConfigFiles[project].setPath(dotConfigPath(project));

    if (dotConfigFiles[project].exists()) {
        dotConfigFiles[project].parse();
        configChanged = dotConfigFiles[project].updateConfig(configPtr->group(KERN_KGROUP));
    }

    if (configChanged)
        importViaDialog(project);

    cacheValidFiles(project);

    KDevKernelUtils::kdevkernelConfigDump(configPtr->group(KERN_KGROUP)); // DEBUG ONLY, DELETE THIS

    return AbstractFileManagerPlugin::import(project);
}

KDevelop::ProjectFileItem * KDevKernel::createFileItem(KDevelop::IProject* project, const KDevelop::Path& path, KDevelop::ProjectBaseItem* parent)
{
    KDevelop::ProjectFileItem* item = new KDevelop::ProjectFileItem(project, path, parent);

    // skip if not in project root
    if (project->path().isDirectParentOf(path)) {
        qInfo() << __LINE__ << path; // DEBUG ONLY, DELETE THIS
        // only create targets for makefile, this is ensure creation only happens once. noted makefile is not parsed for targets
        if (isMakefile(path.lastPathSegment())) {
            if (!isModuleProject(project)) {
                new KDevelop::ProjectTargetItem(project, QString("modules"), parent);
                new KDevelop::ProjectTargetItem(project, QString("vmlinux"), parent);
                new KDevelop::ProjectTargetItem(project, QString("zImage"), parent);
            }
        }
    }

    return item;
}

KDevelop::ProjectFolderItem * KDevKernel::createFolderItem(KDevelop::IProject* project, const KDevelop::Path& path, KDevelop::ProjectBaseItem* parent)
{
    return new KDevelop::ProjectBuildFolderItem( project, path, parent );
}

// TODO need to add more valid files for kernel project, e.g. makefiles, headers, etc.
bool KDevKernel::isValid(const KDevelop::Path& url, const bool isFolder, KDevelop::IProject* project) const
{
    if (isModuleProject(project) && project->path().isParentOf(url))
        return KDevelop::AbstractFileManagerPlugin::isValid(url, isFolder, project);

    // FIXME target display has nothing to do with file validity
    if (project->path().isDirectParentOf(url)) {
        if (isMakefile(url.lastPathSegment()))
            return true;
    }

    KDevelop::Path::List includeList = includeDirectories(project->projectItem());
    foreach (KDevelop::Path path, includeList) {
        if (path == url || path.isParentOf(url))
            return true;
    }

    return makeParser[project]->isValid(url);
}

QHash<QString, QString> KDevKernel::defines(KDevelop::ProjectBaseItem *item) const
{
    KDevelop::IProject *project = item->project();
    return dotConfigFiles[project].defineList();
}

void KDevKernel::cacheValidFiles(KDevelop::IProject *project) {
    KSharedConfigPtr configPtr = project->projectConfiguration();

    dotConfigFiles[project].setPath(dotConfigPath(project));

    qInfo() << __FILE__ << __func__ << dotConfigPath(project); // DEBUG ONLY, DELETE THIS

    if (!dotConfigFiles[project].exists()) {
        if (generateDotConfig(project)) {
            // TODO check this error handling
            qCCritical(PLUGIN_KDEVKERNEL) << "Failed to create .config";
            return;
        }
    }

    dotConfigFiles[project].parse();
    dotConfigFiles[project].updateConfig(configPtr->group(KERN_KGROUP));

    // remove existing makefile parser
    if (makeParser[project] != nullptr)
        delete makeParser[project];
    makeParser[project] = new KMakeParser(configPtr->group(KERN_KGROUP).readEntry(KERN_KDIR), &dotConfigFiles[project]);
}

int KDevKernel::generateDotConfig(KDevelop::IProject* project)
{
    KConfigGroup config(project->projectConfiguration()->group(KERN_KGROUP));
    QString defconfig(config.readEntry(KERN_DEFCONFIG, ""));

    if (defconfig.isEmpty())
        return -1;

    if (defconfig == "default")
        defconfig = "defconfig";
    else if (defconfig == "current config")
        defconfig = "oldconfig";
    else
        defconfig = defconfig + "_defconfig";

    MakeVariables makeVars(makeVarsForProject(project));
    QStringList vars;
    MakeVariables::const_iterator it = makeVars.constBegin();
    while (it != makeVars.constEnd()) {
        vars += QString("%1=%2").arg(it->first).arg(it->second);
        ++it;
    }
    vars += defconfig;
    KProcess *process = new KProcess();
    process->setWorkingDirectory(config.readEntry(KERN_KDIR));
    process->setProgram("make", vars);

    qInfo() << __FILE__ << __LINE__ << process->program(); // DEBUG ONLY, DELETE THIS

    int ret = process->execute();
    delete process;

    return ret;
}

KDevelop::Path::List KDevKernel::includeDirectories (KDevelop::ProjectBaseItem* item) const
{
    KDevelop::Path::List ret;
    KDevelop::IProject *project = item->project();
    KConfigGroup config(project->projectConfiguration()->group(KERN_KGROUP));
    KDevelop::Path kernelPath(config.readEntry(KERN_KDIR));
    KDevelop::Path bDir(config.readEntry(KERN_BDIR, kernelPath.toLocalFile()));

    ret << KDevelop::Path(kernelPath, "include");
    ret << KDevelop::Path(kernelPath, "include/uapi");
    ret << KDevelop::Path(kernelPath, "include/generated/uapi");

    if (bDir != kernelPath) {
        ret << KDevelop::Path(bDir, "include");
        ret << KDevelop::Path(bDir, "include/uapi");
        ret << KDevelop::Path(bDir, "include/generated/uapi");
    }

    if (config.hasKey(KERN_ARCH)) {
        QString arch(config.readEntry(KERN_ARCH));
        KDevelop::Path archPath(kernelPath, "arch");
        ret << KDevelop::Path(kernelPath, QString("arch/%1/include").arg(arch));
        foreach (const QString &machDir, makeParser[project]->machDirs()) {
            ret <<  KDevelop::Path(kernelPath, QString("arch/%1/%2/include").arg(arch).arg(machDir));
        }

        ret << KDevelop::Path(kernelPath, QString("arch/%1/include/generated").arg(arch));
        ret << KDevelop::Path(kernelPath, QString("arch/%1/include/generated/uapi").arg(arch));
        if (bDir != kernelPath) {
            ret << KDevelop::Path(bDir, QString("arch/%1/include/generated").arg(arch));
            ret << KDevelop::Path(bDir, QString("arch/%1/include/generated/uapi").arg(arch));
        }
    }

    //TODO exclude std inc

    return ret;
}

KDevelop::IProjectBuilder *KDevKernel::builder() const
{
    return (KDevelop::IProjectBuilder *)(this);
}

KDevelop::Path KDevKernel::buildDirectory(KDevelop::ProjectBaseItem *item) const
{
    return item->project()->projectItem()->path();
}

QList<KDevelop::ProjectTargetItem *> KDevKernel::targets(KDevelop::ProjectFolderItem *item) const
{
    Q_UNUSED(item);
    QList<KDevelop::ProjectTargetItem*> ret;
    return ret;
}

KDevelop::ProjectTargetItem *KDevKernel::createTarget(const QString &target, KDevelop::ProjectFolderItem *parent)
{
    Q_UNUSED(target)
    Q_UNUSED(parent)
    return nullptr;
}

bool KDevKernel::addFilesToTarget(const QList<KDevelop::ProjectFileItem *> &files, KDevelop::ProjectTargetItem *target)
{
    Q_UNUSED(files);
    Q_UNUSED(target);
    return false;
}

bool KDevKernel::removeTarget(KDevelop::ProjectTargetItem *target)
{
    Q_UNUSED(target);
    return false;
}

bool KDevKernel::removeFilesFromTargets(const QList<KDevelop::ProjectFileItem *> &files)
{
    Q_UNUSED(files);
    return false;
}

KDevelop::Path::List KDevKernel::frameworkDirectories(KDevelop::ProjectBaseItem*) const
{
    return KDevelop::Path::List();
}

bool KDevKernel::hasBuildInfo(KDevelop::ProjectBaseItem* item) const
{
    Q_UNUSED(item)
    return false;
}

QString KDevKernel::extraArguments(KDevelop::ProjectBaseItem* item) const
{
    Q_UNUSED(item)
    return QString();
}

KJob *KDevKernel::install(KDevelop::ProjectBaseItem* item, const QUrl &specificPrefix)
{
    Q_UNUSED(item)
    Q_UNUSED(specificPrefix)
    return 0;
}

KJob *KDevKernel::build(KDevelop::ProjectBaseItem *item)
{
    // for module project, just use make, i.e. leave default target is setting empty
    if (isModuleProject(item->project())) {
        KConfigGroup makeConfig(item->project()->projectConfiguration()->group("MakeBuilder"));
        return jobForTarget(item->project(), makeConfig.readEntry("Default Target").split(" ", QString::SkipEmptyParts));
    }

    // if the item is a target, build it
    if (item->target())
        return jobForTarget(item->project(), QStringList(item->target()->baseName()));

    // build targets contained in item
    if (!item->targetList().isEmpty()) {
        QStringList targetList;
        foreach(auto &target, item->targetList())
            targetList << target->baseName();
        return jobForTarget(item->project(), targetList);
    }

    return nullptr;
}

KJob *KDevKernel::clean(KDevelop::ProjectBaseItem *item)
{
    return jobForTarget(item->project(), QStringList("clean"));
}

KJob *KDevKernel::configure(KDevelop::IProject *project)
{
    return jobForTarget(project, QStringList("xconfig"));
}

KJob *KDevKernel::prune(KDevelop::IProject *project)
{
    return jobForTarget(project, QStringList("mrproper"));
}

// FIXME this is very much useless and  error prone, since dotconfig is sometimes created before the project
// it is not possible to use project specific make to generate the file
KJob *KDevKernel::createDotConfig (KDevelop::IProject *project)
{
    KConfigGroup config(project->projectConfiguration()->group(KERN_KGROUP));
    QString defConfig(config.readEntry(KERN_DEFCONFIG, ""));
    if (defConfig.isEmpty()) return 0;
    return jobForTarget(project, QStringList(defConfig + "_defconfig"));
}

MakeVariables KDevKernel::makeVarsForProject(KDevelop::IProject* project)
{
    MakeVariables makeVars;
    KConfigGroup config(project->projectConfiguration()->group(KERN_KGROUP));

    if (config.hasKey(KERN_BDIR))
        makeVars << QPair<QString, QString>("O", KDevelop::Path(config.readEntry(KERN_BDIR)).toLocalFile());
    if (config.hasKey(KERN_ARCH))
        makeVars << QPair<QString, QString>("ARCH", config.readEntry(KERN_ARCH));
    if (config.hasKey(KERN_CROSS))
        makeVars << QPair<QString, QString>("CROSS_COMPILE", config.readEntry(KERN_CROSS));

    return makeVars;
}

KJob *KDevKernel::jobForTarget(KDevelop::IProject *project, const QStringList &targets)
{
    if (m_builder) {
        return m_builder->executeMakeTargets(project->projectItem(),
                                             targets, makeVarsForProject(project));
    }
    else return 0;
}

QList<KDevelop::IProjectBuilder *> KDevKernel::additionalBuilderPlugins(KDevelop::IProject *project) const
{
    Q_UNUSED(project);

    QList<KDevelop::IProjectBuilder *> ret;
    ret << m_builder;
    return ret;
}


int KDevKernel::perProjectConfigPages() const
{
    return 1;
}

KDevelop::ConfigPage * KDevKernel::perProjectConfigPage(int number, const KDevelop::ProjectConfigOptions& options, QWidget* parent)
{
    if (number == 0) {
        return new KDevKernelPreferences(this, options, parent);
    }
    return nullptr;
}

void KDevKernel::projectClosing(KDevelop::IProject* project)
{
    delete makeParser[project];
    makeParser.remove(project);

    dotConfigFiles.remove(project);
}

bool KDevKernel::isModuleProject(KDevelop::IProject *project) const
{
    KSharedConfigPtr configPtr = project->projectConfiguration();
    return configPtr->group(KERN_KGROUP).readEntry(KERN_MODULE, false);
}

/**
 * .config search path
 * Prioritize build directory, if empty, fall back to kernel source directory,
 * again, if empty, fall back to project root
 */
KDevelop::Path KDevKernel::dotConfigPath(KDevelop::IProject *project)
{
    KSharedConfigPtr configPtr = project->projectConfiguration();

    KDevelop::Path ret = project->path();
    ret = KDevelop::Path(configPtr->group(KERN_KGROUP).readEntry(KERN_KDIR, ret.toLocalFile()));
    ret = KDevelop::Path(configPtr->group(KERN_KGROUP).readEntry(KERN_BDIR, ret.toLocalFile()));

    return ret;
}

bool KDevKernel::isMakefile(const QString& fileName) const
{
    return  ( fileName == QLatin1String("Makefile")
    || fileName == QLatin1String("makefile")
    || fileName == QLatin1String("GNUmakefile")
    || fileName == QLatin1String("BSDmakefile") );
}

bool KDevKernel::isTarget(const KDevelop::Path& path) const
{
    return ((path.lastPathSegment() == "modules")
    || (path.lastPathSegment() == "vmlinux")
    || (path.lastPathSegment() == "zImage"));
}

// needed for QObject class created from K_PLUGIN_FACTORY_WITH_JSON
#include "kdevkernel.moc"
