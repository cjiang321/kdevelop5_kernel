#ifndef KDEVELOP5_KERNEL_H
#define KDEVELOP5_KERNEL_H

#include "kerneldotconfigfile.h"
#include "kmakeparser.h"

#include <interfaces/iplugin.h>


// TODO check the following includes
#include <project/interfaces/ibuildsystemmanager.h>
#include <project/interfaces/iprojectbuilder.h>
#include <project/interfaces/iprojectfilterprovider.h>
#include <project/abstractfilemanagerplugin.h>
#include <kdevelop/makebuilder/imakebuilder.h>
#include <QSet>
#include <QHash>
#include <QFile>
#include <QDateTime>

class KDevKernelSettings;

class KDevKernel
    : public KDevelop::AbstractFileManagerPlugin
    , public KDevelop::IBuildSystemManager
    , public KDevelop::IProjectBuilder
{
    Q_OBJECT
    Q_INTERFACES(KDevelop::IProjectFileManager)
    Q_INTERFACES(KDevelop::IBuildSystemManager)
    Q_INTERFACES(KDevelop::IProjectBuilder)

public:
    // KPluginFactory-based plugin wants constructor with this signature
    KDevKernel(QObject* parent = nullptr, const QVariantList& args = QVariantList());

    ~KDevKernel() override;

    // AbstractFileManagerPlugin interface
    KDevelop::ProjectFolderItem *import(KDevelop::IProject *project) override;

    // IBuildSystemManager interface
    QHash<QString, QString> defines(KDevelop::ProjectBaseItem *item) const override;
    KDevelop::Path::List includeDirectories(KDevelop::ProjectBaseItem *item) const override;
    KDevelop::IProjectBuilder *builder() const override;
    KDevelop::Path buildDirectory(KDevelop::ProjectBaseItem *item) const override;
    QList<KDevelop::ProjectTargetItem *> targets(KDevelop::ProjectFolderItem *item) const override;
    KDevelop::ProjectTargetItem *createTarget(const QString &target, KDevelop::ProjectFolderItem *parent) override;
    bool addFilesToTarget(const QList<KDevelop::ProjectFileItem *> &files, KDevelop::ProjectTargetItem *target) override;
    bool removeTarget(KDevelop::ProjectTargetItem *target) override;
    bool removeFilesFromTargets(const QList<KDevelop::ProjectFileItem *> &files) override;
    KDevelop::Path::List frameworkDirectories(KDevelop::ProjectBaseItem*) const override;
    bool hasBuildInfo(KDevelop::ProjectBaseItem* item) const override;
    QString extraArguments(KDevelop::ProjectBaseItem* item) const override;

    //TODO check if this interface is necessary
    // IProjectBuilder interface
    KJob *install(KDevelop::ProjectBaseItem* item, const QUrl &specificPrefix = {}) override;
    KJob *build(KDevelop::ProjectBaseItem *project) override;
    KJob *clean(KDevelop::ProjectBaseItem *project) override;
    KJob *configure(KDevelop::IProject *project) override;
    KJob *prune(KDevelop::IProject *project) override;
    QList<KDevelop::IProjectBuilder *> additionalBuilderPlugins(KDevelop::IProject *project) const override;

    int perProjectConfigPages() const override;
    KDevelop::ConfigPage* perProjectConfigPage(int number, const KDevelop::ProjectConfigOptions& options, QWidget* parent) override;

    //TODO consider remove this
    KJob *createDotConfig(KDevelop::IProject *project);

    /**
     * Cache valid files in the linux source tree
     */
    void cacheValidFiles(KDevelop::IProject *project);

protected:
    // AbstractFileManagerPlugin interface
    /**
     * A file is valid if it belongs to the list of files that are enabled through the kernel configuration.
     * A directory is valid if it contains any file we are interested in.
     */
    bool isValid(const KDevelop::Path &url, const bool isFolder, KDevelop::IProject *project) const override;
    KDevelop::ProjectFileItem* createFileItem(KDevelop::IProject* project, const KDevelop::Path& path, KDevelop::ProjectBaseItem* parent) override;
    KDevelop::ProjectFolderItem* createFolderItem(KDevelop::IProject* project, const KDevelop::Path& path, KDevelop::ProjectBaseItem* parent = nullptr) override;

    virtual MakeVariables makeVarsForProject(KDevelop::IProject *project);
    virtual KJob *jobForTarget(KDevelop::IProject *project, const QStringList &targets);

private:
    int generateDotConfig(KDevelop::IProject* project);
    bool isModuleProject(KDevelop::IProject *project) const;
    KDevelop::Path dotConfigPath(KDevelop::IProject *project);
    bool isMakefile(const QString& fileName) const;
    bool isTarget(const KDevelop::Path &path) const;

    IMakeBuilder *m_builder;

    QMap<KDevelop::IProject *, KernelDotConfigFile> dotConfigFiles;
    QMap<KDevelop::IProject *, KMakeParser *> makeParser;

private slots:
    void projectClosing(KDevelop::IProject *project);
};

#endif // KDEVELOP5_KERNEL_H
