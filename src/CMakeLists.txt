include_directories(${CMAKE_CURRENT_SOURCE_DIR}/ui)

set(kdevkernel_SRCS
    ui/kdevkernelpreferences.cpp
    ui/kdevkernelimportdialog.cpp
    ui/kdevkerneluibase.cpp
    kdevkernel.cpp
    kmakeparser.cpp
    kerneldotconfigfile.cpp
    utils.cpp
)

ecm_qt_declare_logging_category(kdevkernel_SRCS
    HEADER debug.h
    IDENTIFIER PLUGIN_KDEVKERNEL
    CATEGORY_NAME "kdevelop.plugin.kdevkernel"
)

ki18n_wrap_ui(kdevkernel_SRCS ui/kdevkernelpreferences.ui)
kconfig_add_kcfg_files(kdevkernel_SRCS kdevkernelconfig.kcfgc)

kdevplatform_add_plugin(kdevkernel
    JSON kdevkernel.json
    SOURCES ${kdevkernel_SRCS}
)

target_link_libraries(kdevkernel
    KDev::Interfaces
    KDev::Project
    KDev::OutputView
    KDev::Util
)
