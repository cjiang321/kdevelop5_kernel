/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Chao Jiang <c.jiang@me.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KERNELDOTCONFIG_H
#define KERNELDOTCONFIG_H

#include <util/path.h>
#include <QHash>

class KConfigGroup;

/**
 * @todo write docs
 */
class KernelDotConfigFile
{
public:
    /**
     * Default constructor
     */
    KernelDotConfigFile() = default;

    explicit KernelDotConfigFile(const KDevelop::Path parentPath);

    /**
     * Destructor
     */
    ~KernelDotConfigFile();

    bool setPath(KDevelop::Path parentPath);

    bool exists() const;

    /**
     * Search for a symbol and return its defined value
     */
    QString defines(const QString& symbol) const;

    /**
     * Get the arch obtained from .config
     */
    QString arch() const;

    /**
     * Get the entire list of definitions
     */
    QHash<QString, QString> defineList() const;

    /**
     * Parse .config for a list of definition pairs
     */
    void parse();

    /**
     * Parse the first valid line only to obtain the arch
     */
    void parseArch();

    /**
     * Try to update the config, return true if it changes any configuration, false otherwise
     */
    bool updateConfig(KConfigGroup configGrp);

private:
    KDevelop::Path m_path;
    QHash<QString, QString> m_defines;
    QString m_arch;
};

#endif // KERNELDOTCONFIG_H
